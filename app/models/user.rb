class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
       	validates_uniqueness_of :username

  has_one :profile, dependent: :destroy
  has_many :posts, dependent: :destroy
  has_one :library, dependent: :destroy
  has_many :archives, dependent: :destroy
end
