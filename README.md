*EP3 - SHAREQUILL*

*Autores: Eliseu Kadesh Rosa Assunção Júnior (18/0015834) e Gabriel Paiva Aguiar (18/0016938).*

	O Sharequill é um protótipo de rede social feito como trabalho para a disciplina de Orientação a Objetos, na UNB-FGA, no primeiro semestre de 2019. O objetivo desse programa é servir como área de divulgação de obras literárias para autores amadores, permitindo que tais usuários criem e compartilhem seus textos com outros escritores e leitores.

*- INSTRUÇÕES -*

	Para executar o programa, primeiro clone o repositório (https://gitlab.com/gabriel-paiva/ep3). Em seguida, entre na pasta clonada e execute o comando 'bundle install' (é necessário ter instalado o Ruby com versão igual ou maior que 2.5.1, e a framework Rails com versão igual ou maior que 5.2.3. Para mais detalhes, cheque a Gemfile do programa). A seguir, execute o comando "rails db:migrate" e "rails s".

	Agora é possível acessar a aplicação pelo navegador.

	Tendo acessado a aplicação, é necessário criar uma nova conta como usuário para ter acesso aos seus recursos. Não é possível se cadastrar com um e-mail ou username já existentes. Após inserir as informações de usuário, você deve criar uma descrição para o seu perfil, assim, finalizando o cadastro.

	Com o user e profile criados, o usuário terá acesso à sua biblioteca pessoal, onde poderá criar seus textos (archives). Cada texto criado gera um novo post no Feed de notícias, para divulgação do autor. No feed, também é possível ver as divulgações de todos os outros escritores, sendo possível acessar seus respectivos perfis para ler suas histórias publicadas.
