Rails.application.routes.draw do
  resources :libraries
  resources :archives
  resources :profiles
  devise_for :users, :controllers => {registrations: 'registrations'}
  resources :posts
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root "mainpage#mainpage"
end
