class AddUserIdToArchive < ActiveRecord::Migration[5.2]
  def change
    add_column :archives, :user_id, :integer
  end
end
