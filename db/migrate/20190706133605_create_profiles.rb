class CreateProfiles < ActiveRecord::Migration[5.2]
  def change
    create_table :profiles do |t|
      t.integer :user_id
      t.string :username
      t.string :fullname
      t.text :description

      t.timestamps
    end
  end
end
