class AddFieldsToUser < ActiveRecord::Migration[5.2]
  def change
  	add_column :users, :username, :string
  	add_column :users, :firstname, :string
  	add_column :users, :lastname, :string
  	# Tentativa de deixar o username unico no programa:
  	#add_index :users, :username, unique:true
  end
end
